const {get} = require("../db/connect");


async function insertOTP(email, otp) {
    try {
      // Insert email and OTP into the database
      await get().query('INSERT INTO otp_table (email, otp) VALUES ($1, $2)', [email, otp]);
    } catch (error) {
      console.error('Error inserting OTP into database:', error);
      throw error; // Rethrow the error to handle it in the route handler
    }
  }
  
  async function getOTP(email) {
    try {
      // Query the database to retrieve the stored OTP for the given email
      const result = await get().query('SELECT otp FROM otp_table WHERE email = $1', [email]);
      
      // Extract the OTP from the query result
      const storedOTP = result.rows[0]?.otp;
  
      return storedOTP;
    } catch (error) {
      console.error('Error retrieving OTP from database:', error);
      throw error; // Rethrow the error to handle it in the route handler
    }
  }

  async function deleteOTP(email) {
    try {
      // Delete the OTP from the database for the given email
      await get().query('DELETE FROM otp_table WHERE email = $1', [email]);
    } catch (error) {
      console.error('Error deleting OTP from database:', error);
      throw error; // Rethrow the error to handle it in the route handler
    }
  }
  module.exports = { insertOTP,getOTP,deleteOTP };