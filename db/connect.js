const { Pool } = require("pg");
const connectionString = "postgres://localhost/test";

const pool = new Pool({
  connectionString: connectionString,
});
state = {
  db: null,
};
module.exports.connect = async () => {
  try {
    state.db = await pool.connect();
    console.log("Connected to PostgreSQL");
  } catch (error) {
    console.error("Error connecting to PostgreSQL:", error);
  } finally {
    await pool.end();
  }
};
module.exports.get = () => {
  return state.db;
};
