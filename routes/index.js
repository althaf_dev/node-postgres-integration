var express = require("express");
var router = express.Router();
const { get } = require("../db/connect");
const transporter = require("../services/emailService");
const generateOTP = require("../services/otpServices");
const { insertOTP, getOTP, deleteOTP } = require("../helpers/storeOTP");

router.get("/", async function (req, res, next) {
  res.render("index", { title: "Express" });
});

router.post("/send-otp", async function (req, res, next) {
  const { email } = req.body;
  req.session.email = email;
  console.log("email::", email);
  try {
    const result = await get().query("SELECT * FROM test_table2");
    const otp = generateOTP();
    insertOTP(email, otp);
    console.log(result);
    const mailOptions = {
      from: "zxyabc511@gmail.com",
      to: email,
      subject: "Test OTP",
      text: `Here's your one-time password (OTP) to access: OTP - ${otp}`,
    };

    // Send mail
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.error("Error sending email:", error);
      } else {
        console.log("Email sent:", info.response);
        res.render("otp-send-success");
      }
    });
  } catch (error) {
    console.error("Error executing query:", error);
    res.status(500).json({ error: "Internal server error" });
  }
  // res.render('index', { title: 'Express' });
});

router.post("/verify-otp", async function (req, res, next) {
  const { otp } = req.body;
  const email = req.session.email;
  try {
    // Retrieve stored OTP from the database
    const storedOTP = await getOTP(email);
    console.log(storedOTP, otp);
    // Compare the submitted OTP with the stored OTP
    if (otp === storedOTP) {
      console.log("verification done");
      await deleteOTP(email);

      // Clear the email from the session
      // delete req.session.email;

      res.render("access-granted");
    } else {
      res.status(400).json({ success: false, message: "Incorrect OTP" });
    }
  } catch (error) {
    console.error("Error verifying OTP:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});
module.exports = router;
